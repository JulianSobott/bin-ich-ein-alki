FROM python:3.11

ENV PYTHONUNBUFFERED=0

COPY poetry.lock pyproject.toml /app/
WORKDIR /app
RUN pip install poetry && poetry config virtualenvs.create false && poetry install --no-dev

COPY . .

ENV PYTHONPATH=/app

# uvicorn
CMD ["uvicorn", "web:app", "--host", "0.0.0.0", "--port", "8000"]
EXPOSE 8000
