# fast api web server
import json

import pandas as pd
from fastapi import FastAPI, File, UploadFile
from fastapi.responses import HTMLResponse
from jinja2 import Environment, FileSystemLoader

app = FastAPI()
env = Environment(loader=FileSystemLoader("templates"))


@app.get("/", response_class=HTMLResponse)
async def root():
    template = env.get_template("index.html")
    return template.render()


# handle file upload
@app.post("/upload", response_class=HTMLResponse)
async def upload(file: UploadFile = File(...)):
    data = json.loads(file.file.read())
    df = pd.DataFrame(data)
    positions = await get_positions(df)
    center = await get_center(df)
    glass_type_histogram = await get_histogram_glass_type(df)
    time_histogram = await get_histogram_time(df)
    template = env.get_template("analysis.html")
    return template.render(
        alc_map={"heatmap": positions, "center": center, "zoom": 5},
        glass_type_histogram=glass_type_histogram,
        time_histogram=time_histogram
    )


async def get_positions(df: pd.DataFrame):
    df_pos = df[df["longitude"].notna()]
    return df_pos[["latitude", "longitude"]].values.tolist()


async def get_center(df: pd.DataFrame):
    df_pos = df[df["longitude"].notna()]
    center_lat = df_pos["latitude"].max() - (df_pos["latitude"].max() - df_pos["latitude"].min()) / 2
    center_lon = df_pos["longitude"].max() - (df_pos["longitude"].max() - df_pos["longitude"].min()) / 2
    return [center_lat, center_lon]


async def get_histogram_glass_type(df: pd.DataFrame):
    sorted_df = df.groupby("glassType").size().sort_values(ascending=False)
    labels = sorted_df.index.tolist()
    data = sorted_df.tolist()
    return {
        "labels": labels,
        "data": data
    }


async def get_histogram_time(df: pd.DataFrame):
    df["hour"] = df["timestamp"].apply(lambda x: int(x[11:13]))
    sorted_df = df.groupby("hour").size()
    labels = sorted_df.index.tolist()
    data = sorted_df.tolist()
    return {
        "labels": labels,
        "data": data
    }


if __name__ == '__main__':
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
