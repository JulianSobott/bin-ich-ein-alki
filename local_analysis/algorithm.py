import json
import pandas as pd
from folium.plugins import HeatMap
import folium
file = "history_Gesamt-nils.json"
with open(file, "r") as f:
    data = json.load(f)
df = pd.DataFrame(data)
df_pos = df[df["longitude"].notna()]
center_lat = df_pos["latitude"].max() - (df_pos["latitude"].max() - df_pos["latitude"].min()) / 2
center_lon = df_pos["longitude"].max() - (df_pos["longitude"].max() - df_pos["longitude"].min()) / 2
m = folium.Map(location=[center_lat, center_lon], zoom_start=5)
# create a heatmap of longitude and latitude
HeatMap(df_pos[["latitude", "longitude"]], radius=12, max_zoom=20).add_to(m)
m.save("heatmap.html")
